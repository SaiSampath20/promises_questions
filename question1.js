const fs = require('fs');

function promises() {
    return new Promise((reject, resolve) => {
        fs.readFile("./data.txt", "utf-8", (err, data) => {
            if (err) {
                resolve(err);
            } else {
                reject(data);
            }
        })
    })
}

promises()
    .then(data => console.log(data))
    .catch(err => console.log(err.message))
    // It will run successfully