const fs = require('fs');

function promises() {
    return new Promise((resolve, reject) => {
        fs.readFile("./file.txt", "utf-8", (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    })
}

promises()
    .then(a => console.log(a), b => console.log("In then", b))
    .catch(err => console.log("error"))
    // It will run successfully
    // output : In then and error will be printed
    /* If you will send two callbacks in then block it will take first callback as then and other one callback as catch */