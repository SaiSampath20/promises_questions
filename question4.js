const fs = require("fs");

function promises() {
    return new Promise((resolve, reject) => {
        fs.readFile("./file.txt", "utf-8", (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    })
}

promises()
    .then((data) => console.log(data))
    .then((data) => {
        console.log(data);
        return new Error("nothing");
    })
    .then((data) => console.log("In then block", data))
    .catch((err) => console.log(err));

/* output:
something;
undefined
In then block Error: nothing
at /home/sampath/Documents/node/promises_questions/question4.js:19:16 
*/
/* return new Error will return error to then block i.e. line 21 not to catch block */