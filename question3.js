const fs = require("fs");

function promises() {
    return new Promise((resolve, reject) => {
        fs.readFile("./file.txt", "utf-8", (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
    })
}

promises()
    .catch((err) => console.log(err.message))
    .then((data) => console.log(data))
    .then((data) => console.log(data))
    .then((data) => console.log(data));
/* 
    something;
    undefined
    undefined
    */